package bookstoreHibernate;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {

    public static void main(String[] args) {

        Logger.getLogger("org.hibernate").setLevel(Level.SEVERE);
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("bookstore_Hibernate");
        EntityManager entityManager = factory.createEntityManager();

//        Author author = entityManager.createQuery("from Author where lastName = :name", Author.class)
//                .setParameter("name", "Sapkowski")
//                .getSingleResult();
//        author.books.forEach(System.out::println);

        Category categories = entityManager.createQuery("from Category where name = :name", Category.class)
                .setParameter("name", "Horror")
                .getSingleResult();
        categories.books.forEach(System.out::println);
    }
}
