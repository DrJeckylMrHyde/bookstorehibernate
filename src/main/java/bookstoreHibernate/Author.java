package bookstoreHibernate;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(name = "authors")
public class Author {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;
    @Column(name = "first_name")
    public String firstName;
    @Column(name = "last_name")
    public String lastName;
    @Column(name = "date_of_birth")
    public LocalDate dateOfBirth;
    @Enumerated(EnumType.STRING)
    public Sex sex;

    @ManyToMany
    @JoinTable(name = "books_authors",
//            w joinColumns laduje kolumna wlasciciela relacji
            joinColumns = @JoinColumn(name = "author_id"),
//            w inverseJoinColumns laduje druga
    inverseJoinColumns = @JoinColumn(name = "book_id"))
    List<Book> books;

    @Override
    public String toString() {
        return "Author{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", sex=" + sex +
                ", books=" + books +
                '}';
    }
}
