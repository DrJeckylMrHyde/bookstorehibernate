package bookstoreHibernate;

import javax.persistence.*;
import java.math.BigInteger;
import java.util.List;

@Entity
@Table(name = "books")
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    String title;

    @Column(name = "pages_number")
    Integer pagesNumber;
    Long isbn;

    @ManyToOne()
    @JoinColumn(name = "category_id")
//            nazwa tego pola moze byc dowolna
    Category category;

    @ManyToMany(mappedBy = "books")
    List<Author> authors;

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", pagesNumber=" + pagesNumber +
                ", isbn=" + isbn +
                ", category=" + category +
                '}';
    }
}
