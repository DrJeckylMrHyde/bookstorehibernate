package bookstoreHibernate;

import javax.persistence.*;

@Entity
@Table(name = "copies")
public class Copy {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Integer id;
    @ManyToOne
    @JoinColumn(name = "book_id")
    Book book;

    @ManyToOne
    @JoinColumn(name = "format_id")
    Format format;
    Double cost;

    @Override
    public String toString() {
        return "Copy{" +
                "id=" + id +
                ", book=" + book +
                ", format=" + format +
                ", cost=" + cost +
                '}';
    }
}
